from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
configuration = huetainment_api.Configuration()
configuration.host = "http://rpi-2.lan:3001/api/v1"
api_instance = huetainment_api.StatesApi(huetainment_api.ApiClient(configuration))

try:
    # Get Settings
    api_response = api_instance.get_states()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SettingsApi->get_settings: %s\n" % e)

print("State is %s" % (api_response.capturing and api_response.syncing) ? "ON" : "OFF")
