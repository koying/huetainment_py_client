# huetainment_api.StatesApi

All URIs are relative to *https://virtserver.swaggerhub.com/Semperpax/huetainment/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_states**](StatesApi.md#get_states) | **GET** /states | Get states
[**set_states**](StatesApi.md#set_states) | **PUT** /states | Set states


# **get_states**
> States get_states()

Get states

### Example
```python
from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = huetainment_api.StatesApi()

try:
    # Get states
    api_response = api_instance.get_states()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatesApi->get_states: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**States**](States.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_states**
> set_states(state_object=state_object)

Set states

### Example
```python
from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = huetainment_api.StatesApi()
state_object = huetainment_api.States() # States | State to set (optional)

try:
    # Set states
    api_instance.set_states(state_object=state_object)
except ApiException as e:
    print("Exception when calling StatesApi->set_states: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state_object** | [**States**](States.md)| State to set | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

