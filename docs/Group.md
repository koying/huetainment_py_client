# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_streaming** | **bool** | Is currently streaming? | [optional] 
**name** | **str** | Group name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


