# Settings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capture_device** | **str** |  | [optional] 
**capture_interval** | **int** | in ms | [optional] 
**capture_multiplier** | **int** | Resolution of the capture (1 &#x3D; 16x9) | [optional] 
**capture_video_standard** | **str** |  | [optional] 
**center_slowness** | **float** |  | [optional] 
**chroma_boost** | **float** | in percent | [optional] 
**luma_boost** | **float** | in percent | [optional] 
**color_bias** | **int** | in percent | [optional] 
**min_luminance** | **float** | in percent | [optional] 
**max_luminance** | **float** | in percent | [optional] 
**side_slowness** | **float** |  | [optional] 
**sat_threshold** | **float** |  | [optional] 
**val_threshold** | **float** |  | [optional] 
**syncing_group** | **str** | The Hue entertainment group | [optional] 
**capture_margins** | **list[int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


