# States

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capture_device** | **str** |  | [optional] 
**syncing_group** | **str** | The Hue entertainment group | [optional] 
**capturing** | **bool** | Capturing? | [optional] 
**syncing** | **bool** | Syncing? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


