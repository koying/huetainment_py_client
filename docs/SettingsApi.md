# huetainment_api.SettingsApi

All URIs are relative to *https://virtserver.swaggerhub.com/Semperpax/huetainment/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_settings**](SettingsApi.md#get_settings) | **GET** /settings | Get Settings
[**set_settings**](SettingsApi.md#set_settings) | **PUT** /settings | Set settings


# **get_settings**
> Settings get_settings()

Get Settings

### Example
```python
from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = huetainment_api.SettingsApi()

try:
    # Get Settings
    api_response = api_instance.get_settings()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SettingsApi->get_settings: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Settings**](Settings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_settings**
> set_settings(setting_object=setting_object)

Set settings

### Example
```python
from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = huetainment_api.SettingsApi()
setting_object = huetainment_api.Settings() # Settings | Settings to se (optional)

try:
    # Set settings
    api_instance.set_settings(setting_object=setting_object)
except ApiException as e:
    print("Exception when calling SettingsApi->set_settings: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setting_object** | [**Settings**](Settings.md)| Settings to se | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

