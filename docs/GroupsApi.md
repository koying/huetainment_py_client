# huetainment_api.GroupsApi

All URIs are relative to *https://virtserver.swaggerhub.com/Semperpax/huetainment/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_groups**](GroupsApi.md#get_groups) | **GET** /groups | Get Entertainment groups


# **get_groups**
> list[Group] get_groups()

Get Entertainment groups

### Example
```python
from __future__ import print_function
import time
import huetainment_api
from huetainment_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = huetainment_api.GroupsApi()

try:
    # Get Entertainment groups
    api_response = api_instance.get_groups()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->get_groups: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Group]**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

