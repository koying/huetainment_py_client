# coding: utf-8

"""
    huetainment API

    huetainment API  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: cbro@semperpax.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import huetainment_api
from huetainment_api.models.setting_luma_boost import SettingLumaBoost  # noqa: E501
from huetainment_api.rest import ApiException


class TestSettingLumaBoost(unittest.TestCase):
    """SettingLumaBoost unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testSettingLumaBoost(self):
        """Test SettingLumaBoost"""
        # FIXME: construct object with mandatory attributes with example values
        # model = huetainment_api.models.setting_luma_boost.SettingLumaBoost()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
